package com.irtika.demo;

import com.irtika.demo.dao.UserDataBase;
import com.irtika.demo.dao.UserDbRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserItemWriter implements ItemWriter<UserDataBase> {
    @Autowired
    UserDbRepository userRepository;

    @Override
    public void write(List<? extends UserDataBase> list) throws Exception {
        System.out.println();
//        userRepository.saveAll(list);

    }
}
