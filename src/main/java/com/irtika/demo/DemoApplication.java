package com.irtika.demo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class DemoApplication {
	@Autowired
	JobLauncher jobLauncher;
	@Autowired
	Job job;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
