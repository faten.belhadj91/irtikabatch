package com.irtika.demo.service;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

//@Controller
  class JobLauncherUser {
    @Autowired
    JobLauncher jobLauncher;
    @Autowired
    Job job;

    public BatchStatus launch() throws Exception {
        Map<String, JobParameter> parameterMap = new HashMap<>();
        JobParameters jobParameters = new JobParameters(parameterMap);
        JobExecution jobExecution = jobLauncher.run(job,jobParameters);
        return jobExecution.getStatus();
    }

}
