package com.irtika.demo;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.HashMap;
import java.util.Map;
//@RestController
public class JobRestController {
    @Autowired
    JobLauncher jobLauncher;
    @Autowired
    Job job;

//    @GetMapping("/startJob")
    public BatchStatus load() throws Exception{
        Map<String, JobParameter> parameterMap = new HashMap<>();
        JobParameters jobParameters = new JobParameters(parameterMap);
        JobExecution jobExecution = jobLauncher.run(job,jobParameters);
        return jobExecution.getStatus();
    }
}
