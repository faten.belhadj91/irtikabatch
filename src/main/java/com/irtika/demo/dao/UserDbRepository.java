package com.irtika.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

public interface UserDbRepository extends JpaRepository <UserDataBase, BigInteger>{
}
