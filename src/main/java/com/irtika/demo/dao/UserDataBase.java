package com.irtika.demo.dao;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name="public.users")
public class UserDataBase {
    @Id
    @Column(name="id",nullable = false)
    private BigInteger id;
    private String address;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String phoneNumber;
    private String userName;
    private String roleId;
    private String classId;
    private String gender;


}
