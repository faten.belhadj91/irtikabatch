package com.irtika.demo.dao;

import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private BigInteger id;
    private String nom;
    private String prenom;
    private String username;
    private String DbPassword;
    private String password;
    private String classe;


}
